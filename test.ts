import {bsrcrypto as crypto} from "./index";

let text = "TEST";
let key = "1234";
console.log("Original Input:");
console.log(text);
let enc = crypto.encrypt(text,key);
console.log("Encrypted:");
console.log(enc);
console.log("Then Decrypted:");
let dec = crypto.decrypt(enc,key);
console.log(dec);

