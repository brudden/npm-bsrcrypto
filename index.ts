import * as crypto from "crypto";

export class bsrcrypto {
    static preferredCryptoAlgorithm = "aes-256-cbc";

    static encrypt(text:string, key:string, alg=this.preferredCryptoAlgorithm) {
        let cipher = crypto.createCipher(alg,key);
        let crypted = cipher.update(text,'utf8','hex');
        crypted += cipher.final('hex');
        return crypted;
    }

    static decrypt(text:string, key:string, alg=this.preferredCryptoAlgorithm) {
        var decipher = crypto.createDecipher(alg,key);
        var dec = decipher.update(text,'hex','utf8');
        dec += decipher.final("utf8");
        return dec;
    }
}
