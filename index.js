var crypto = require("crypto");
var bsrcrypto = (function () {
    function bsrcrypto() {
    }
    bsrcrypto.encrypt = function (text, key, alg) {
        if (alg === void 0) { alg = this.preferredCryptoAlgorithm; }
        var cipher = crypto.createCipher(alg, key);
        var crypted = cipher.update(text, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    };
    bsrcrypto.decrypt = function (text, key, alg) {
        if (alg === void 0) { alg = this.preferredCryptoAlgorithm; }
        var decipher = crypto.createDecipher(alg, key);
        var dec = decipher.update(text, 'hex', 'utf8');
        dec += decipher.final("utf8");
        return dec;
    };
    bsrcrypto.preferredCryptoAlgorithm = "aes-256-cbc";
    return bsrcrypto;
})();
exports.bsrcrypto = bsrcrypto;
//# sourceMappingURL=index.js.map