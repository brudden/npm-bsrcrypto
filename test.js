var index_1 = require("./index");
var text = "TEST";
var key = "1234";
console.log("Original Input:");
console.log(text);
var enc = index_1.bsrcrypto.encrypt(text, key);
console.log("Encrypted:");
console.log(enc);
console.log("Then Decrypted:");
var dec = index_1.bsrcrypto.decrypt(enc, key);
console.log(dec);
//# sourceMappingURL=test.js.map